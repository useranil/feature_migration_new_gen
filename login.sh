#!/bin/bash
cookiesfile="cookies/cookies$1.txt"
commonparams="--insecure --cookie-jar $cookiesfile --silent --show-error"

jazzlogin(){
	curl $commonparams "${hostbase}secure/authenticated/identity" --output /dev/null
	curl $commonparams --location --cookie $cookiesfile --data j_username=$jazzusername --data j_password=$jazzpassword ${hostbase}secure/authenticated/j_security_check --output /dev/null
}
jazzlogin
