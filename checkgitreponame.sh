#/bin/bash
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID >&2
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi
gitdir="git-data/$streamUUID/$componentUUID/.git"

pushd $gitdir/.. 1>/dev/null
git fetch --quiet 2>/dev/null
if [ $? -eq 0 ]
then
	echo "OK $streamUUID $componentUUID" 
else
	echo "ERROR $streamUUID/$componentUUID"
fi
popd 1>/dev/null
