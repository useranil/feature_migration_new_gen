#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi
export streamUUID
export componentUUID

. ./credentials.sh
parallel=$COMMONS/parallel/bin/parallel
mkdir -p data cookies
changesetsfilename="data/changesets-$streamUUID-$componentUUID.txt"
downloadsfile="data/tobedownloaded-$streamUUID-$componentUUID.txt"
echo 1. Get changset list
. ./getchangesets.sh
echo 2. Get the fullchangeset of each
rm -f $downloadsfile
cat $changesetsfilename|$parallel --eta -j1 env changesetUUID={} ./getfullchangeset.sh
echo 3. Download files
cat  $downloadsfile|$parallel --eta -j5  ./mycurl.sh {} -$streamUUID-$componentUUID{%}
echo 4. Get metadata
cat $changesetsfilename|$parallel --eta  env changesetUUID={} ./getmetadata.sh
