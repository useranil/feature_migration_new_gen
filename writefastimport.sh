#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID >&2
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID >&2
	exit 1
fi
if [ "$citnetenvironment" = "" ]
then
        echo ERROR no CITnet environment defined in citnetenvironment >&2
        exit 1
fi
existing="data/existingfiles-$streamUUID-$componentUUID.txt"
rm -f $existing
touch $existing
addtoexisting(){
        if [ $(grep "^$1$" $existing |wc -l) -eq 0 ]
        then
                echo "$1">>$existing
        fi
}

deletefromexisting(){
	sed -i "/^$(echo $1|sed 's/\//\\\//g')/d" $existing
}
renameinexisting(){
	sed -i "s/^$(echo $1|sed 's/\//\\\//g')/$(echo $2|sed 's/\//\\\//g')/" $existing
}
modify(){
	if [ -f "$1" ]
	then
		echo "M 100644 inline $2"
		if [ $(gzip -t $1 2>/dev/null) ]
		then
			echo data $(gzip -l $1 |tail -n 1|sed -r 's/ +/ /g'|cut -f3 -d' ')
			zcat "$1"
		else
			echo data $(stat -c '%s' "$1") 
			cat "$1"
		fi
		echo
	else
		echo "M 100644 inline $2"
		echo "data 0"
	fi
	addtoexisting $2
}

delete(){
	echo "D $1"
	deletefromexisting $1
}

renamedir(){
        if [ "$1" != "$2" ]
        then
                if [ $(grep "^$1$" $existing |wc -l) -eq 0 ]
                then
                        echo "#DEBUG rename missing source dir in existing list"
                        echo "#R $1 $2"
                else
                        echo "#DEBUG for rename grep ^$1$ $existing "
                        grep "^$1$" $existing|sed 's/^/#/'
                        echo "R $1 $2"
                        renameinexisting $1 $2
                fi
        else
                echo "#DEBUG rename for the same name happened, bad code smells! "
        fi	
}

rename(){
	if [ "$1" != "$2" ]
	then 
		if [ $(grep "^$1$" $existing |wc -l) -eq 0 ]
		then
			echo "#DEBUG rename missing source file in existing list, handling as modify"
			echo "#R $1 $2"
			modify "$1" "$2"
		else
			echo "#DEBUG for rename grep ^$1$ $existing "
			grep "^$1$" $existing|sed 's/^/#/'
			echo "R $1 $2"
			renameinexisting $1 $2
		fi
	else
		echo "#DEBUG rename for the same name happened, bad code smells! "
	fi
}

commit(){
	
echo "commit refs/heads/master"
cat "data/$streamUUID/$componentUUID/metadata-$changesetUUID.txt" | sed 's/$/ +0000/'
commentfilename="data/$streamUUID/$componentUUID/$citnetenvironment/comment-$changesetUUID.txt"
echo data $(stat -c '%s' "$commentfilename")
cat "$commentfilename"
}




cat "data/changesets-$streamUUID-$componentUUID.txt" |while read changesetUUID;do
	resultfilename="data/$streamUUID/$componentUUID/fullchangeset-$changesetUUID.txt"
	commit
	cat $resultfilename|sed 's/%/%%/g'|awk -f sortchangeset.awk|while IFS='|' read changeType afterPath afterStateId itemType itemId path ; do
	downloadedfilename="jazz-data/$streamUUID/$componentUUID/$changesetUUID/$itemId.gz"
	echo "#$changeType $afterPath $afterStateId $itemType $itemId $path"
	case $changeType in
		"0"|"1")#added
			case $itemType in
				"com.ibm.team.scm.Folder")
					#nothing to do
					;;
				"com.ibm.team.filesystem.FileItem")
					if [ "$afterStateId" != "" ]
					then
						modify "$downloadedfilename" "$afterPath" 
					else
						echo "#WARNING afterStateId is missing! ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID"
					fi
					;;
				*)
					echo "#ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
					;; 
			esac
			;;
		"2"|"6")#modified
			case $itemType in
				"com.ibm.team.scm.Folder")
					#nothing to do
					;;
				"com.ibm.team.filesystem.FileItem")
					if [ "$afterStateId" != "" ]
					then
						if [ "$afterPath" != "$path" ]
						then
							rename "$path" "$afterPath"
						fi
						modify "$downloadedfilename" "$afterPath" 
					else
						echo "#WARNING afterStateId is missing! ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID"
					fi
					;;
				*)
					echo "#ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
					;; 
			esac
			;;
		"4"|"8"|"12")#moved
			case $itemType in	
				"com.ibm.team.scm.Folder")
					renamedir "$path" "$afterPath"
				;;
				"com.ibm.team.filesystem.FileItem")			
					if [ "$afterStateId" != "" ]
					then
						if [ "$afterPath" != "$path" ]
                                                then
							delete "$path"
						fi
						modify "$downloadedfilename" "$afterPath"
					 else
						echo "#WARNING afterStateId is missing! ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID"
					fi
				;;
				*)
					echo "#ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
				;;
			esac
			;;
		"10"|"14")#moved and modified
			case $itemType in
				"com.ibm.team.filesystem.FileItem")
					if [ "$afterPath" != "$path" ]
					then
						delete "$path"
					fi
					modify "$downloadedfilename" "$afterPath" 
					;;
				*)
					echo "#ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
					;; 
			esac
			;;
		"16")#deleted
			case $itemType in
				"com.ibm.team.scm.Folder")
					 delete "$path"	
					;;
				"com.ibm.team.filesystem.FileItem")
					 delete "$path"	
					;;
				*)
					echo "#ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
					;; 
			esac
			;;
		*)
			echo "#ERROR unknown changetype $changeType ($changeType $afterPath $afterStateId $itemType $itemId $path),in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
			;; 
	esac
	done
done


