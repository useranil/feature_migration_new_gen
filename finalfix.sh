#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID >&2
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID >&2
	exit 1
fi
if [ "$citnetenvironment" = "" ]
then
        echo ERROR no CITnet environment defined in citnetenvironment >&2
        exit 1
fi
if [ "$citnetprojectkey" = "" ]
then
        echo ERROR no CITnet projectkey defined in citnetprojectkey >&2
        exit 1
fi
case $citnetenvironment in
        "test"|"tst")
                gitbaseurl="https://webgate.test.ec.europa.eu/CITnet/stash/scm"
        ;;
        "acc")
                gitbaseurl="https://webgate.acceptance.ec.europa.eu/CITnet/stash/scm"
        ;;
        "prod"|"prd")
                gitbaseurl="https://webgate.ec.europa.eu/CITnet/stash/scm"
        ;;
        *)

esac

gitdir="git-data/$streamUUID/$componentUUID/.git"
lastversiondir="lastversion-data/$streamUUID/$componentUUID/"
hostbase="https://s-cc-rwp01.net1.cec.eu.int:9443/jazz/"
export https_proxy=
scm=$COMMONS/jazz/scmtools/eclipse/scm.sh
visrc="visrc-$streamUUID-$componentUUID"
. ./credentials.sh
echo "login into RTC Jazz"
$scm login -u $jazzusername -P $jazzpassword -n rtc -r $hostbase
echo "deleting workspace before"
$scm delete workspace -r rtc "$visrc"
echo "creating workspace"
$scm create workspace -r rtc --empty  "$visrc"
echo "adding flow target"
$scm add flowtarget -r rtc  "$visrc" "$streamUUID"
echo "adding component to workspace"
$scm add component -r rtc -s "$streamUUID" "$visrc" "$componentUUID"
echo "before cleaning data folder"
rm -rf "$lastversiondir"
mkdir -p "$lastversiondir"
echo "loading sandbox"
$scm load  -r rtc  -d "$lastversiondir" "$visrc"
echo "deleting workspace after"
$scm delete workspace -r rtc "$visrc"
cp -r $gitdir $lastversiondir
rm -rf ${lastversiondir}.jazz5 ${lastversiondir}.metadata
pushd $lastversiondir
git add --all
git commit --author="$citnetgitauthor" --message "Fixed the final diff between RTC and the migrated."
git tag "RTC2CITnet-migration-finalfix"
git push origin master
git push --tags
popd
