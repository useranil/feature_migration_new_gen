#!/bin/bash
#streamUUID the UUID of the stream
#changesetUUID the UUID of the changeset
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID
	exit 1
fi

if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi

if [ "$changesetUUID" = "" ]
then
	echo ERROR no changeset defined in changesetUUID
	exit 1
fi

cookiesfile="cookies/cookies-fullchangeset-$streamUUID-$componentUUID.txt"

commonparams="--insecure --cookie-jar $cookiesfile --silent --show-error"

jazzlogin(){
	curl $commonparams "${hostbase}secure/authenticated/identity" --output /dev/null
	curl $commonparams --location --cookie $cookiesfile --data j_username=$jazzusername --data j_password=$jazzpassword ${hostbase}secure/authenticated/j_security_check --output /dev/null
}
jazzgetfullchangeset() {
	curl $commonparams --cookie $cookiesfile -H 'accept: text/json' ${hostbase}'service/com.ibm.team.filesystem.common.internal.rest.IFilesystemRestService2/changeSetPlus?changeSetPath=workspaceId%2F'$1'%2FchangeSetId%2F'$2
	if [ $? -ne 0 ]
	then
		echo "ERROR curl failed to download. code: $? ${hostbase}service/com.ibm.team.filesystem.common.internal.rest.IFilesystemRestService2/changeSetPlus?changeSetPath=workspaceId%2F$1%2FchangeSetId%2F$2()"
	fi
}
jazzdownloadfile(){
	if [ ! -f $downloadfilename ]
	then
		mkdir -p $(dirname "$downloadfilename")
		echo "--insecure --cookie-jar cookies.txt --silent --show-error --location --cookie cookies.txt ${hostbase}service/com.ibm.team.filesystem.common.IFileContentService/content/$1/$2 --output $downloadfilename" >>$tobedownloaded
	fi
}
jsondir="jazz-json/$streamUUID/$componentUUID"
fullchangesetfilename="$jsondir/fullchangeset-$changesetUUID.json"
mkdir -p "data/$streamUUID/$componentUUID"
resultfilename="data/$streamUUID/$componentUUID/fullchangeset-$changesetUUID.txt"
if [ ! -f $fullchangesetfilename ] || [ ! -f $resultfilename ]
then
	jazzlogin
	jazzgetfullchangeset $streamUUID $changesetUUID > $fullchangesetfilename
	
	cat $fullchangesetfilename|/ec/local/citnet/commons/jq/jq  --raw-output '."soapenv:Body".response.returnValue.value.changeSetDTO.changes[]|map_values(tostring)|.changeType+"|"+.afterPath+"|"+.afterStateId+"|"+.itemType+"|"+.itemId+"|"+.path' > $resultfilename
fi

tobedownloaded="data/tobedownloaded-$streamUUID-$componentUUID.txt"
cat $resultfilename|while IFS='|' read changeType afterPath afterStateId itemType itemId path ; do
downloadfilename="jazz-data/$streamUUID/$componentUUID/$changesetUUID/$itemId.gz"
case $changeType in
	"0"|"1")#added
		case $itemType in
			"com.ibm.team.scm.Folder")
				#nothing to do/download
				;;
			"com.ibm.team.filesystem.FileItem")
				jazzdownloadfile $itemId $afterStateId 
				;;
			*)
				echo "ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
				;; 
		esac
		;;
	"2"|"6")#modified
		case $itemType in
			"com.ibm.team.filesystem.FileItem")
				if [ "$afterStateId" != "" ]
				then
					jazzdownloadfile $itemId $afterStateId 
				else
					echo "WARNING afterStateId is missing! ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID"
				fi
				;;
			"com.ibm.team.scm.Folder")
				#nothing to do/download
				;;
			*)
				echo "ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
				;; 
		esac
		;;
	"4"|"8"|"12")#moved
		case $itemType in
			"com.ibm.team.filesystem.FileItem")
				jazzdownloadfile $itemId $afterStateId
				;;
			"com.ibm.team.scm.Folder")
				#nothing to do/download
				;;
		       *)
				echo "ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
				;;
		esac
		;;
	"10"|"14")#moved and modified
		case $itemType in
			"com.ibm.team.filesystem.FileItem")
				jazzdownloadfile $itemId $afterStateId 
				;;
			*)
				echo "ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
				;; 
		esac
		;;
	"16")#deleted
		case $itemType in
			"com.ibm.team.scm.Folder")
				#nothing to do download
				;;
			"com.ibm.team.filesystem.FileItem")
				#nothing to do download
				;;
			*)
				echo "ERROR unknown item type $itemType ($changeType $afterPath $afterStateId $itemType $itemId $path), in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
				;; 
		esac
		;;
	*)
		echo "ERROR unknown changetype $changeType ($changeType $afterPath $afterStateId $itemType $itemId $path),in changeset $changesetUUID in component $componentUUID in stream $streamUUID "
		;; 
esac
done
