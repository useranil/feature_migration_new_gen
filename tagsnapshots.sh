#/bin/bash
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID >&2
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi
gitdir="git-data/$streamUUID/$componentUUID/.git"
baselinesfile="../../../data/baselines-$componentUUID.txt"
snapshotsfile="../../../data/snapshots-$streamUUID.txt"

pushd $gitdir/..
git tag|xargs -n1 git tag -d 
git fetch
git tag |grep --invert-match RTC2CITnet-migration|xargs -n1 git push origin --delete
git tag|grep --invert-match RTC2CITnet-migration|xargs -n1 git tag -d 
cat $snapshotsfile | while IFS='|' read tagname baselineUUID; do
	cat $baselinesfile | grep $baselineUUID | while IFS='|' read notused1 tagdate notused2; do
		tagcommit=$(git log  --date=unix --format="%ad|%H" --author-date-order |awk -F"|" '$1<'$tagdate'{print $0;exit}'|cut -f2 -d'|')
		tagname=$(echo $tagname|sed 's/ /_/g')
		#echo "DEBUG tagname=$tagname tagdate=$tagdate tagcommit=$tagcommit"
		git tag -f "$tagname" "$tagcommit"
	done
done
git push origin --tags
popd
