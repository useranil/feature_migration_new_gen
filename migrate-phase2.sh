#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID
	exit 1
fi
export streamUUID
export componentUUID

. ./credentials.sh
parallel=$COMMONS/parallel/bin/parallel
mkdir -p data cookies
changesetsfilename="data/changesets-$streamUUID-$componentUUID.txt"
downloadsfile="data/tobedownloaded-$streamUUID-$componentUUID.txt"
echo 5. Lookup workitems in JIRA
./lookupworkiteminjira.sh
echo 6. Replace WIs to JIRA keys in comments
cat $changesetsfilename|$parallel --eta  env changesetUUID={} ./convertcomment.sh
