package to.rtc.cli.migrate.util;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import groovy.io.FileType;

/**
 * original @author christoph.erni
 * modified to execute in groovy by Istvan Verhas
 */
public class JazzignoreTranslator {
	private static final Pattern EXCLUSION = Pattern.compile("\\{(.*?)\\}");

	/**
	* Translates a .jazzignore file to .gitignore
	* 
	* @param jazzignore
	*            the input .jazzignore file
	* 
	* @return the translation, as a list of lines
	*/
	public static List<String> toGitignore(File jazzignore) {
		List<String> gitignoreLines = new ArrayList<String>();
		try {
			String lineForRegex = "";
			boolean needToTransform = false;
			jazzignore.eachLine {line ->
				line = line.trim();
				if (!line.startsWith("#")) {
					needToTransform = true;
					lineForRegex += line;
					if (!line.endsWith("\\")) {
						addGroupsToList(lineForRegex, gitignoreLines);
						lineForRegex = "";
						needToTransform = false;
					}
				}
			}
			if (needToTransform) {
				gitignoreLines = addGroupsToList(lineForRegex, gitignoreLines);
			}
		} catch (IOException ioe) {
			throw new RuntimeException("unable to read .jazzignore file " + jazzignore.getAbsolutePath(), ioe);
		}
		return gitignoreLines;
	}

	private static List<String> addGroupsToList(String lineToMatch, List<String> ignoreLines) {
		boolean recursive = lineToMatch.startsWith("core.ignore.recursive");
		Matcher matcher = EXCLUSION.matcher(lineToMatch);
		while (matcher.find()) {
			String pattern = (!recursive ? "/" : "").concat(matcher.group(1));
			ignoreLines.add(pattern);
		}
		return ignoreLines;
	}
}
startdir = (args as List)[0] ? args[0]: '.'
println "Looking for jazz ignore files starting from $startdir"

new File(startdir).eachFileRecurse(FileType.FILES){ jazzignorefile ->
	if (jazzignorefile.name.equals(".jazzignore")){
		gitignorefile= new File(jazzignorefile.getParent()+"/.gitignore")
		println "gitignorefilename is $gitignorefile"
		parentPath=jazzignorefile.getParent().replaceFirst("^\\.","")+"/"
		JazzignoreTranslator.toGitignore(jazzignorefile).each{
			line=it.replaceFirst("^/",parentPath)
			gitignorefile << "$line\n"
		}
	}
}

