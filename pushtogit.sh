#!/bin/bash
#streamUUID the UUID of the stream
#componentUUID the UUID of the component
if [ "$streamUUID" = "" ]
then
	echo ERROR no stream defined in streamUUID >&2
	exit 1
fi
if [ "$componentUUID" = "" ]
then
	echo ERROR no component defined in componentUUID >&2
	exit 1
fi
if [ "$citnetenvironment" = "" ]
then
        echo ERROR no CITnet environment defined in citnetenvironment >&2
        exit 1
fi
if [ "$citnetprojectkey" = "" ]
then
        echo ERROR no CITnet projectkey defined in citnetprojectkey >&2
        exit 1
fi
case $citnetenvironment in
        "test"|"tst")
                gitbaseurl="https://webgate.test.ec.europa.eu/CITnet/stash/scm"
        ;;
        "acc")
                gitbaseurl="https://webgate.acceptance.ec.europa.eu/CITnet/stash/scm"
        ;;
        "prod"|"prd")
                gitbaseurl="https://webgate.ec.europa.eu/CITnet/stash/scm"
        ;;
        *)

esac
gitdir="git-data/$streamUUID/$componentUUID/.git"
if [ "$citnetrepopostfix" = "" ]
then
	gitreponame=$(echo "$1 - $2"|sed 's/ /-/g')
else
	gitreponame=$(echo "$1 - $2 - $citnetrepopostfix"|sed 's/ /-/g')
fi
pushd $gitdir/..
echo "The remote repository to push to is $gitbaseurl/$citnetprojectkey/$gitreponame.git"
git remote add origin $gitbaseurl"/$citnetprojectkey/$gitreponame.git"
if [ !$? ]
then
	git remote set-url origin $gitbaseurl"/$citnetprojectkey/$gitreponame.git"
fi
git push -u origin --all
popd
